import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    admin: boolean = false;
    public company_id: number;
    types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
    rows: Array<any>;
    InfoModal: any;
    deleteModal: any;
    companyToDelete: any;
    orderInfo: any;

    constructor(public api: ApiService, public activatedRoute: ActivatedRoute, private router: Router, public modalService: NgbModal) { }

    ngOnInit() {
        this.admin = localStorage.getItem('type') == '0';
        this.company_id = -1;
        this.getCompanyOrders( this.company_id);
    }

    getCompanyOrders(company_id) {
        this.api.sendPost('webGetCompanyFirstDishOrders', {'company_id':  this.company_id,}).subscribe(data =>
        {
            this.rows = data;
            console.log("webGetCompanyOrders111",data);
        });
    }

    neworderpage() {
        this.router.navigate(['/','company_firstdish_order','create'], { queryParams: { company_id: this.company_id } });
    }

    goEditPage(order_id,company_id) {
        this.router.navigate(['/','company_firstdish_order','edit'], { queryParams: { company_id: company_id,order_id: order_id } });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.rows.filter(function(d) {
            return d.fullname && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteCompany(){
        this.api.sendPost('webDeleteCompanyFirstDishOrder', {id: this.companyToDelete.id}).subscribe(data => {
            this.getCompanyOrders(this.company_id);
        });
        this.deleteModal.close();
    }

    openInfoModal(content, company) {
        this.InfoModal = this.modalService.open(content);
        this.orderInfo = company;
    }



    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }

}
