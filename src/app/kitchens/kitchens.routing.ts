import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';
import {ShowComponent} from './show/show.component';

export const KitchensRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Kitchens'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New kitchen'},
        },
        {
            path: ':id',
            component: ShowComponent,
            data: {heading: ''},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Edit kitchen'},
        }
    ]
}];
