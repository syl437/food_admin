import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

    public loading = false;
    kitchens: Array<any> = [];
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        address: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        price: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        same_day: new FormControl(1, Validators.required),
        same_day_hour: new FormControl(9, Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];
    hours = [];

    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService) {}

    async ngOnInit() {
        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.description.setValue(quill.root.innerHTML);
        });

        for (let i = 1; i <= 24; i++){
            let name = String(i);
            if (i === 12){
                name = '12 בצהריים';
            }
            if (i === 24) {
                name = '12 בלילה (ללא הגבלה)';
            }
            this.hours.push({value: i, name: name});
        }
    }

    changeSettings () {
        if (this.form.value.same_day == 1){
            this.form.addControl('same_day_hour', new FormControl(9, Validators.required));
        }
        if (this.form.value.same_day == 0){
            this.form.removeControl('same_day_hour');
        }
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null){
            this.errors.push({message: "Logo is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.address = this.form.value.address;
        payload.email = this.form.value.email;
        payload.username = this.form.value.username;
        payload.password = this.form.value.password;
        payload.price = this.form.value.price;
        payload.description = this.form.value.description;
        payload.same_day = this.form.value.same_day == true ? '1' : '0';
        if (this.form.value.same_day){
            payload.same_day_hour = this.form.value.same_day_hour;
        }
        payload.image = this.logo.file;

        console.log(payload);
        this.api.sendPost('webAddKitchen', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/kitchens']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the kitchen wasn't created"});

        });

    }


}