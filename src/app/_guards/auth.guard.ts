import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('id')) {
            switch(localStorage.getItem('type')){
                case '0':
                    return true;
                case '1':
                    if (state.url !== '/companies/' + localStorage.getItem('id') &&
                        state.url !== '/companies/edit/' + localStorage.getItem('id') ){
                        this.router.navigate(['/companies/' + localStorage.getItem('id')]);
                        return true;
                    }
                    return true;
                case '2':
                    if (state.url !== '/kitchens/' + localStorage.getItem('id') &&
                        state.url !== '/kitchens/edit/' + localStorage.getItem('id') &&
                        !state.url.startsWith('/kitchens/' + localStorage.getItem('id') + '/categories')){
                        this.router.navigate(['/kitchens/' + localStorage.getItem('id')]);
                        return true;
                    }
                    return true;
                default:
                    return true;
            }
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication']);
        return false;
    }
}