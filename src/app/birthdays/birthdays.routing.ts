import {Routes} from '@angular/router';
import {IndexComponent} from "./birthdays.component";

export const BirthdaysRoute: Routes = [{
    path: '',
    children: [{
        path: '',
        component: IndexComponent,
        data: {heading: 'Birthdays'},
    }
    ]
}];
