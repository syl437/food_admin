import {Component, OnInit} from '@angular/core';
import {ApiService} from '../_services/api/api.service';

@Component({
    selector: 'app-birthdays',
    templateUrl: './birthdays.component.html',
    styleUrls: ['./birthdays.component.css']
})
export class IndexComponent implements OnInit {

    employees;
    rows;

    constructor(public api: ApiService) {}

    ngOnInit() {
        this.api.sendGet('webGetBirthdays').subscribe(data => {
            this.employees = data;
            this.rows = data;
        });
    }

}
