import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const CustomMealRoutes: Routes = [{
    path: '',
    children: [
        {
            path: ':id',
            component: IndexComponent,
            data: {heading: 'הרכבת הזמנה'},
        }
    ]
}];



