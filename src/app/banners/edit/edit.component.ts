import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {

    public loading = false;
    banner;
    banner_id;
    form: FormGroup = this.fb.group({
        page: new FormControl('', Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];

    constructor(public fb: FormBuilder,
                public activatedRoute: ActivatedRoute,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService) {}

    async ngOnInit() {

        this.activatedRoute.params.subscribe(params => {
            this.banner_id = params.id;
                this.api.sendPost('webGetBanner', {banner_id: this.banner_id}).subscribe(data => {

                    this.banner = data;
                    this.form.setValue({page: this.banner.page});

                    this.logo.path = this.banner.picture;

            });
        });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }


    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null && this.logo.path == ''){
            this.errors.push({message: "Logo is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.id = this.banner.id;
        payload.page = this.form.value.page;
        if (this.logo.file){
            payload.image = this.logo.file;
        }

        console.log(payload);
        this.api.sendPost('webUpdateBanner', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/banners']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the banner wasn't updated"});

        });

    }

}