import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';

export const BannersRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Banners'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New banner'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Edit banner'},
        }
    ]
}];
