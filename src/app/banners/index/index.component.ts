import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    banners: Array<any>;
    deleteModal: any;
    itemToDelete: any;

    constructor(public api: ApiService, public modalService: NgbModal, public activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.getBanners();
    }

    getBanners () {
        this.api.sendPost('webGetBanners', {}).subscribe(data => {
            this.rows = data;
            this.banners = this.rows;
        });
    }

    async deleteItem(){
        this.api.sendPost('webDeleteBanner', {banner_id: this.itemToDelete.id}).subscribe(data => {
            this.getBanners();
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    updateFilter (event) {

    }

}
