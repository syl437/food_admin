import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
    public loading = false;
    days: Array<any> = [
        {itemName: 'יום ראשון', id: 0},
        {itemName: 'יום שני', id: 1},
        {itemName: 'יום שלישי', id: 2},
        {itemName: 'יום רביעי', id: 3},
        {itemName: 'יום חמישי', id: 4},
        {itemName: 'יום שישי', id: 5},
        {itemName: 'שבת', id: 6},
    ];
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        days: new FormControl(null, Validators.required),
        description: new FormControl('', Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];
    dropdownSettingsDays = {
        singleSelection: false,
        text:"Select days",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item"
    };
    kitchen_id;
    category_id;
    dish_id;
    dish;

    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute,
                public api: ApiService) {}

    ngOnInit() {

        this.activatedRoute.params.subscribe(async (params) => {
            this.kitchen_id = params.id;
            this.category_id = params.category_id;
            this.dish_id = params.dish_id;

            this.api.sendPost('webGetDish', {dish_id: this.dish_id}).subscribe(data => {
                this.dish = data.dish;

                let days = [];
                for (let day of data.days){
                    let d:any = {};
                    d.id = Number(day.day);
                    d.itemName = '';
                    for (let item of this.days){
                        if (item.id == d.id){
                            d.itemName = item.itemName;
                        }
                    }
                    days.push(d);
                }

                this.form.setValue({
                    name: this.dish.name,
                    days: days,
                    description: this.dish.desc
                });

                this.logo.path = environment.apiEndpoint + this.dish.image;
            })
        });

    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null && this.logo.path == ''){
            this.errors.push({message: "Picture is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.id = this.dish.index;
        payload.name = this.form.value.name;
        payload.description = this.form.value.description;
        payload.image = this.logo.file;
        payload.days = this.form.value.days.map(day => {return day.id});

        console.log(payload);
        this.api.sendPost('webUpdateDish', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/kitchens/' + this.kitchen_id + '/categories/' + this.category_id + '/dishes']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the dish wasn't updated"});

        });

    }

}