import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

    public loading = false;
    days: Array<any> = [
        {itemName: 'יום ראשון', id: 0},
        {itemName: 'יום שני', id: 1},
        {itemName: 'יום שלישי', id: 2},
        {itemName: 'יום רביעי', id: 3},
        {itemName: 'יום חמישי', id: 4},
        {itemName: 'יום שישי', id: 5},
        {itemName: 'שבת', id: 6},
    ];
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        days: new FormControl(null, Validators.required),
        description: new FormControl('', Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];
    dropdownSettingsDays = {
        singleSelection: false,
        text:"Select days",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item"
    };
    kitchen_id;
    category_id;

    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute,
                public api: ApiService) {}

    ngOnInit() {
        this.activatedRoute.params.subscribe(async (params) => {
            this.kitchen_id = params.id;
            this.category_id = params.category_id;
        });

        // const quill = new Quill('#editor-container', {
        //     modules: {toolbar: {container: '#toolbar-toolbar'}},
        //     theme: 'snow'
        // });
        //
        // quill.on('text-change', () => {
        //     this.form.controls.description.setValue(quill.root.innerHTML);
        // });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null){
            this.errors.push({message: "Picture is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.kitchen_id = this.kitchen_id;
        payload.type = this.category_id;
        payload.name = this.form.value.name;
        payload.description = this.form.value.description;
        payload.image = this.logo.file;
        payload.days = this.form.value.days.map(day => {return day.id});

        console.log(payload);
        this.api.sendPost('webAddDish', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/kitchens/' + this.kitchen_id + '/categories/' + this.category_id + '/dishes']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the dish wasn't created"});

        });

    }

}