import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    kitchen_id;
    category_id;
    rows: Array<any>;
    dishes: Array<any>;
    deleteModal: any;
    itemToDelete: any;

    constructor(public api: ApiService, public modalService: NgbModal, public activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(async (params) => {
            this.kitchen_id = params.id;
            this.category_id = params.category_id;
            this.getDishes(this.kitchen_id, this.category_id);
        })
    }

    getDishes(kitchen_id, category_id){
        this.api.sendPost('webGetDishes', {kitchen_id: kitchen_id, category_id: category_id}).subscribe(data => {
            this.rows = data;
            this.dishes = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.dishes.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteDish(){
        this.api.sendPost('webDeleteDish', {dish_id: this.itemToDelete.index}).subscribe(data => {
            this.getDishes(this.kitchen_id, this.category_id);
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}
