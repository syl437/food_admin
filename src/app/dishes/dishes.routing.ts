import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';

export const DishesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Dishes'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New dish'},
        },
        {
            path: 'edit/:dish_id',
            component: EditComponent,
            data: {heading: 'Edit dish'},
        }
    ]
}];
