import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id;
    rows: Array<any>;
    employees: Array<any>;
    deleteModal: any;
    itemToDelete: any;

    constructor(public api: ApiService, public modalService: NgbModal, public activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data.id;
            this.getEmployees(this.company_id);
        })

    }

    getEmployees (id) {
        this.api.sendPost('webGetEmployees', {company_id: id}).subscribe(data => {
            this.rows = data;
            this.employees = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.employees.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        this.api.sendPost('webDeleteEmployee', {employee_id: this.itemToDelete.index}).subscribe(data => {
            this.getEmployees(this.company_id);
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}
