import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

    public loading = false;
    company_id;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        address: new FormControl(''),
        email: new FormControl(''),
        idcard: new FormControl('', Validators.required),
        manager: new FormControl(0, Validators.required),
        description: new FormControl(''),
        birthday: new FormControl(''),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];

    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute,
                public api: ApiService) {}

    async ngOnInit() {
        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.description.setValue(quill.root.innerHTML);
        });

        this.activatedRoute.params.subscribe((data) => this.company_id = data['id']);

    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        this.loading = true;

        let payload: any = {};
        payload.company_id = this.company_id;
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.address = this.form.value.address;
        payload.email = this.form.value.email;
        payload.idcard = this.form.value.idcard;
        payload.manager = this.form.value.manager;
        payload.description = this.form.value.description;
        payload.birthday = this.form.value.birthday;
        if (this.logo.file){
            payload.image = this.logo.file;
        }
        console.log(payload);

        this.api.sendPost('webAddEmployee', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/companies/' + this.company_id + '/employees']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the employee wasn't created"});

        });

    }


}