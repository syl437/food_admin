import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {

    public loading = false;
    company_id;
    employee_id;
    employee;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        address: new FormControl(''),
        email: new FormControl(''),
        idcard: new FormControl('', Validators.required),
        manager: new FormControl(0, Validators.required),
        description: new FormControl(''),
        birthday: new FormControl(''),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];

    constructor(public fb: FormBuilder,
                public activatedRoute: ActivatedRoute,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService) {}

    async ngOnInit() {

        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.description.setValue(quill.root.innerHTML);
        });

        this.activatedRoute.parent.params.subscribe(parentParams => {
            this.company_id = parentParams.id;
            this.activatedRoute.params.subscribe(async (data) => {

                this.employee_id = data['id'];

                this.api.sendPost('webGetEmployee', {employee_id: this.employee_id}).subscribe(data => {

                    this.employee = data;
                    this.form.setValue({
                        name: this.employee.name,
                        phone: this.employee.phone,
                        address: this.employee.address,
                        email: this.employee.email,
                        idcard: this.employee.idcard,
                        manager: Number(this.employee.manager),
                        description: this.employee.desc,
                        birthday: this.employee.birthday,
                    });

                    this.logo.path = environment.apiEndpoint + this.employee.image;
                    quill.clipboard.dangerouslyPasteHTML(this.employee.desc);
                });
            });
        });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }


    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null && this.logo.path == ''){
            this.errors.push({message: "Logo is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.id = this.employee.index;
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.address = this.form.value.address;
        payload.email = this.form.value.email;
        payload.idcard = this.form.value.idcard;
        payload.manager = Number(this.form.value.manager);
        payload.description = this.form.value.description;
        payload.birthday = this.form.value.birthday;
        if (this.logo.file){
            payload.image = this.logo.file;
        }

        console.log(payload);
        this.api.sendPost('webUpdateEmployee', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/companies/' + this.company_id + '/employees']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the employee wasn't updated"});

        });

    }

}