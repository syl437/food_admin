import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';
import {ShowComponent} from './show/show.component';

export const CompaniesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Companies'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New company'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Edit company'},
        },
        {
            path: ':id',
            component: ShowComponent,
            data: {heading: ''},
        }
    ]
}];
