import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    kitchen_id;

    constructor(public api: ApiService, public router: Router, public activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(data => {
            this.kitchen_id = data.id;
        })
    }

    goInside (category){
        console.log(this.kitchen_id, category);
        this.router.navigate(['/kitchens/' + this.kitchen_id + '/categories/' + category + '/dishes']);
    }

}
